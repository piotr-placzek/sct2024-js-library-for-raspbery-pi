import { Gpio, BinaryValue as GpioBinaryValueType, Low as GpioLowType, High as GpioHighTyp } from 'onoff';

/**
 * Pulse width in miliseconds
 */
enum T {
    T_CLK = 10, // min: 20ns
    T_SDI = 10, // min: 100ns
    T_LA = 10,  // min: 180ns
    T_OE = 10,  // min: 20ns
}

interface CurrentStates {
    clk_s: BinaryValue;
    sdi_s: BinaryValue;
    la_s: BinaryValue;
    oe_s: BinaryValue;
}

export type Low = GpioLowType;
export type High = GpioHighTyp;
export type BinaryValue = GpioBinaryValueType;

export default class SCT2024_RPI {
    private __CLK__: Gpio;
    private __SDI__: Gpio;
    private __LA__: Gpio;
    private __OE__: Gpio;
    private __state__: CurrentStates;

    /**
     * Constructor
     * @param gpio_clk 
     * @param gpio_sdi 
     * @param gpio_la 
     * @param gpio_oe 
     */
    constructor(
        gpio_clk: number,
        gpio_sdi: number,
        gpio_la: number,
        gpio_oe: number
    ) {
        this.__state__ = {
            clk_s: 0,
            sdi_s: 0,
            la_s: 0,
            oe_s: 1,
        }

        this.__CLK__ = new Gpio(gpio_clk, 'out');
        this.__SDI__ = new Gpio(gpio_sdi, 'out');
        this.__LA__ = new Gpio(gpio_la, 'out');
        this.__OE__ = new Gpio(gpio_oe, 'out');

        this.__setup__();
    }

    /**
     * Set up initial gpio states
     */
    private __setup__() {
        function error_handler(e: Error | null | undefined) {
            if (e) {
                throw e;
            }
        }

        this.__CLK__.write(this.__state__.clk_s, error_handler);
        this.__SDI__.write(this.__state__.sdi_s, error_handler);
        this.__LA__.write(this.__state__.la_s, error_handler);
        this.__OE__.write(this.__state__.oe_s, error_handler);
    }

    /**
     * Helper for writing `BinaryValue | number` values as `BinaryValue`
     * @param gpio 
     * @param value 
    */
    private __gpio_write__({ gpio, value }: { gpio: Gpio; value: BinaryValue | number; }): Promise<any> {
        return new Promise(
            (resolve: any, reject: any) => {
                gpio.write(value as BinaryValue, (error: any) => {
                    if(error) reject(error);
                    else resolve();
                });
            }
        );
    }

    /**
     * Generate peek on `gpio` for `time` duration
     * @param gpio 
     * @param time 
     */
    private __gpio_peek__({ gpio, time, self = this }: { gpio: Gpio; time: T; self?: SCT2024_RPI; }): Promise<any> {
        let state = 0;
        async function executor(resolve: any, reject: any): Promise<any> {
            async function task(): Promise<any> {
                state ^= 1;
                try {
                    await self.__gpio_write__({gpio, value: state});
                }
                catch (error) {
                    reject(error);
                }
        
                if(!state) {
                    resolve();
                }
            }

            await task();
            if(state) setTimeout(task, time);
        }

        return new Promise(executor);
    }

    /**
     * Generate single clock pulse
     * @param self 
     */
    private __clkTick__(self: SCT2024_RPI = this): Promise<any> {
        return self.__gpio_peek__({ gpio: self.__CLK__, time: T.T_CLK });
    }


    /**
     * Write single value to SDI
     * @param state 
     * @param self
     */
    private __write__({ state, self = this }: { state: BinaryValue; self?: SCT2024_RPI; }): Promise<any> {
        return self.__gpio_write__({ gpio: self.__SDI__, value: state })
            .then(() => self.__clkTick__(self));
    }

    /**
     * Write set of values to SDI
     * @param state 
     * @param self
     */
    private __write_array__({ state, self = this }: { state: BinaryValue[]; self?: SCT2024_RPI; }): Promise<any> {
        let p: Promise<any> = Promise.resolve();
        let error = null
        for (let i = 0; i < state.length && !error; i++) {
            p = p
                .then(_ => self.__write__({ state: state[i], self }))
                .catch(
                    (e: any) => {
                        error = e;
                    }
                );
        }

        return p.then(() => Promise.resolve).catch((e: any) => Promise.reject(e));
    }

    /**
     * Write data (Set SDI and tick CLK)
     * @param state
     */
    write(state: BinaryValue | BinaryValue[]): Promise<any> {
        const self = this;
        if (Array.isArray(state)) return this.__write_array__({ state, self });
        else return this.__write__({ state, self });
    }

    /**
     * Latch data
     */
    latch(): Promise<any> {
        return this.__gpio_peek__({ gpio: this.__LA__, time: T.T_LA });
    }

    /**
     * Set output enabled
     */
    enable_output(): Promise<any> {
        this.__state__.oe_s = 0;
        return this.__gpio_write__({ gpio: this.__OE__, value: 0 });
    }

    /**
     * Set output disabled
     */
    disable_output(): Promise<any> {
        this.__state__.oe_s = 1;
        return this.__gpio_write__({ gpio: this.__OE__, value: 1 });
    }
}

