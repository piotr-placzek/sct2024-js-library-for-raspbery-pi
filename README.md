# SCT2024 JS library for Raspbery Pi

SCT2024CSSG 16-bit Serial-In/Parallel-Out Constant-Current LED driver controller for your Raspberry PI.

# API

_*The BinaryValue type comes from the 'onoff' module_

- `constructor(gpio_clk: number, gpio_sdi: number, gpio_la: number, gpio_oe: number)`
- `write(state: BinaryValue | BinaryValue[]): Promise<any>`
- `enable_output(): Promise<any>`
- `disable_output(): Promise<any>`
- `latch(): Promise<any>`
