const SCT2024 = require("RP-SCT2024").default;

const GPIO_CLK = 22;
const GPIO_SDI = 23;
const GPIO_LA = 24;
const GPIO_OE = 27;

const sct_out = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0];
const driver = new SCT2024(GPIO_CLK, GPIO_SDI, GPIO_LA, GPIO_OE);


driver.write(sct_out)
    .then( () => driver.latch() )
    .then( () => driver.enable_output() )
    .catch(
        (e) => { console.log(e); }
    );
